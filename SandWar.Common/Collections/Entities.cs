﻿using System;
using System.Collections.Generic;
using System.Linq;
using SandWar.Common.Entities;

namespace SandWar.Common.Collections
{
	/// <summary>Thread safe collection of entities.</summary>
	public class Entities<T> where T : EntityData
	{
		readonly Dictionary<long, T> entities = new Dictionary<long, T>();
		
		/// <summary>Add an entity to this collection.</summary>
		/// <param name="data">The entity.</param>
		public void Add(T data) {
			lock (entities)
				entities.Add(data.ID, data);
		}
		
		/// <summary>Remove an entity from this collection.</summary>
		/// <param name="id">The ID of the entity.</param>
		/// <returns><c>true</c> on success.</returns>
		public bool Remove(long id) {
			lock (entities)
				return entities.Remove(id);
		}
		
		/// <summary>Get an entity by it's ID.</summary>
		/// <param name="id">The ID of the entity.</param>
		/// <returns>The entity or <c>null</c>.</returns>
		public T ById(long id) {
			T entity;
			lock (entities)
				if (!entities.TryGetValue(id, out entity))
					entity = null;
			return entity;
		}
		
		/// <summary>Remove all entities in this collection.</summary>
		public void Clear() {
			lock (entities)
				entities.Clear();
		}
		
		/// <summary>Take a snapshot of this collection at the current status.</summary>
		public IEnumerable<T> Snapshot {
			get { lock (entities) return entities.Values.ToList(); }
		}
	}
}
