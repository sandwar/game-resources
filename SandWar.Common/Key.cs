﻿using System;

namespace SandWar.Common
{
	/// <summary>High level list of keys.</summary>
	public enum Key : byte
	{
		NoKey = 0,
		
		/// <summary>Left mouse click.</summary>
		PrimaryAction = 1,
		/// <summary>Right mouse click.</summary>
		SecondaryAction
	}
}
