﻿using System;
using System.Linq;

namespace SandWar.Common
{
	public static class ServerProperties
	{
		static readonly bool isDevelopmentEnvironment = AppDomain.CurrentDomain.GetAssemblies().Any(assembly => assembly.GetType("UnityEditor.EditorWindow") != null);
		
		public static readonly ushort ServerPort = 17794;
		
		public static readonly string ServerAddress = /*isDevelopmentEnvironment ? "localhost" :*/ "sbw2d.mooo.com";
	}
}
