﻿using System;

namespace SandWar.Common
{
	public enum NetworkCommand : byte
	{
		DoNothing = 0,

		MessagesBundle = 1,
		
		//ServiceCommand = 2,
		
		//ReservedForFuture = 3...6,

		YourPlayerAdd = 7,
		PlayerAdd,
		PlayerWalk,
		TransformSetLocation,
		DestroyEntity,
		PickupAdd,
		CollectPickup,
		AddToInventory = CollectPickup,
		LookAt,
		CreatureSetLookingAt = LookAt,
		ModelAdd,
		BulletAdd,
		PerformAction
	}
}
