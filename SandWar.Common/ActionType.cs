﻿using System;

namespace SandWar.Common
{
	public enum ActionType : byte
	{
		NoAction = 0,
		
		/// <summary>Action starting, like pressing a button.</summary>
		ActionStart = 1,
		/// <summary>Action interrupting, like releasing a button.</summary>
		ActionEnd
	}
}
