﻿using System;

namespace SandWar.Common
{
	public enum Model : uint
	{
		NoModel = 0,
		
		//ReservedForFuture = 1...15,
		
		#region Debug
		
		// Items
		
		DebugBoxRed = 16,
		DebugBoxGreen,
		DebugBoxBlue,
		
		// Creatures
		
		DebugCreatureCapsule,
		
		// Models
		
		DebugModelPracticeTarget,
		
		// Bullets
		
		DebugBulletTracing
		
		#endregion
	}
}
