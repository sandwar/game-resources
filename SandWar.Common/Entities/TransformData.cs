﻿using System;
using SandWar.Math;

namespace SandWar.Common.Entities
{
	public class TransformData : EntityData
	{
		public Vector3 Location;
		public Vector3 Direction;
	}
}
