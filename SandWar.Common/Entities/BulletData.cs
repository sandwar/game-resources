﻿using System;

namespace SandWar.Common.Entities
{
	public class BulletData : TransformData
	{
		public float Speed;  // m/s
		public float Life;  // s
	}
}
