﻿using System.Collections.Generic;
using SandWar.Math;

namespace SandWar.Common.Entities
{
	public class CreatureData : TransformData
	{
		public List<ItemData> Inventory = new List<ItemData>();
		public Vector3 LookingAt;
	}
}
